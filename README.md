# NixOS

This repository serves as my attempt to help others learn about NixOS.

The `docs` folder of the `main` branch will contain the mdbook which is hosted at [https://bytepen.gitlab.io/edu/nixos](https://bytepen.gitlab.io/edu/nixos).

The branches starting with `edu-` will serve as references used within the above mdbook to help show how specific configurations may look.
