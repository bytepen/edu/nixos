# Multi-Box

Branch: [edu-flake-multibox-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-multibox-basic)

Prerequisites to Understand:
  * [edu-flake-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-basic) ([Writeup](./basic.md))

## What is this branch?

This branch shows you how you might expand the [Flake - Basic](./basic.md) branch in order to have the configuration manager multiple different boxes.

This repo defines the configuration for 3 separate boxes; primero, segundo, and tercero.
For simplicity sake, the following is assumed:

* All 3 boxes have the same partition/subvolume schemes:
  * 2 partitions (1 small fat32 for /boot, and 1 large btrfs for /)
  * All have subvolumes for home, nix, persist, swap, and var/log

That said, the drives will all have different UUIDs for their `/boot` and `/` partitions, and for the sake of mixing things up, `tercero` is using an AMD processor instead of an Intel processor.
Additionally, just to show that you don't need it, `hardware-configuration.nix` has been removed from `segundo` and `tercero`, while some of it has been left in `primero`'s `hardware-configuration.nix` which is imported by `primero`'s `default.nix`.

## Where to start?

Make sure you understand everything from [configuration.nix - Basic](../configuration/basic.md) and [Flakes - Basic](./basic.md) and this configuration shouldn't require TOO much mnetal gymnastics to understand.

As always, it's a Flake-based Configuration, so we're going to start by understanding `flake.nix` and seeing how it pulls in `hosts/{hostname}/default.nix`, then we're going to see how that pulls in `common/default.nix`.

## flake.nix

There is one new configuration change here when compared to [Flakes - Basic[(./basic.md).
That's the usage of a mkHost function within our `nixosConfigurations` output.

This may look a little confusing because of the newly introduced `let ... in` syntax.
This syntax effectively lets you create variables which will be used in the next part of the expression.

In this case, we have similar to the following:

```
nixosConfigurations = let
  mkHost = system: hostname: nixpkgs.lib.nixosSystem {
    system = "${system}";
    modules = [ ./hosts/${hostname} ];
  };
in {
  primero = mkHost "x86_64-linux" "primero"
};
```

This can be compared directly against our `nixosConfigurations` from our [Flakes - Basic](./basic.md) configuration, which looks like the following:

```
nixosConfigurations.primero = nixpkgs.lib.nixosSystem {
  system = "x86_64-linux";
  modules = [ ./hosts/primero ];
};
```

Where this comes in handy is that now, all we need to do to define a new system is to add lines like the following:

```
  segundo = mkHost "x86_64-linux" "segundo";
  tercero = mkHost "x86_64-linux" "tercero";
```

This is because our newly created `mkHost` function takes in two arguments (system and hostname from `system: hostname:`) and outputs the `nixpkgs.lib.nixosSystem` as is normally expected to exist in the `nixosConfigurations.<hostname>` outputs.

## hosts/{hostname}/default.nix

You'll notice that all 3 boxes have a default.nix at hosts/{hostname}/default.nix.
The biggest difference between these files is the different hostnames, however, I also had a few changes between each of them to show how things might look different between a couple of boxes in the real world.
The main differences are that `primero` still has a `hardware-configuration.nix`, which is imported by the `default.nix`, whereas that file has been removed from `segundo` and `tercero`, with the necessary options just being set in `default.nix`.

## common

You will also notice that all 3 `hosts/{hostname}/default.nix`'s import `../../common`, which you can see contains `common/default.nix`.

I am using the `common` folder to contain configurations which will be the same across all machines, whereas `hosts/{hostname}/default.nix` contained configurations which were specific to that one box.

As has been explained before, the `default.nix` file is read when you specify a directory as an import.
This `default.nix` sets a couple more options, as well as imports the `boot.nix` and `filesystems.nix` files.

I felt this was a good place to start further exploring the concept of splitting configurations into different files based on what they do.
As such, `boot.nix` contains configuration options which relate to booting, where `filesystems.nix` contains configurations relating to the partitioning scheme shared by all boxes.
