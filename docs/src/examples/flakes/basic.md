# Basic

Branch: [edu-flake-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-basic)

Prerequisites to Understand:
  * [edu-configuration-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-configuration-basic) ([Writeup](../configuration/basic.md))

## What is this branch?

There are so many ways that you could configure Flakes and NixOS in general, however, this branch aims to demonstrate a layout that I personally believe to be pretty straight forward and serves as a good basis for understanding how Flakes work in relation to NixOS configurations.

I have already done a pretty thorough write-up of how this example compares to the [configuration.nix - Basic](../configuration/basic.md) example, so to avoid simply repeating myself, I encourage you to review that information [here](../../getting-started/layouts/flakes.md).

## flake.nix

Within Flake-based NixOS configurations, `flake.nix` serves as the source of truth.

Since one huge benefit that comes with Flakes is the easy management of multiple boxes with a shared set of Configuration files, you will often find references to all of the boxes which are configured by the current Configuration in this folder.
In this case of this specific repository, there is only one box with the hostname `primero`.

I also encourage you to compare this configuration to the one in [Flakes - Multi-box](./multi-box.md) so you can see how it would easily be expanded in order to provide a config for several boxes instead of just the one.

## hosts/{hostname}/default.nix

Within the `hosts` directory, a directory for the only box that's configured by this example (primero) exists.
Within that directory, there is a `default.nix` file, which is nearly identical to the `configuration.nix` file from [configuration.nix - Basic](../configuration/basic.md).

This file could be used to fully configure the primero box, but doing so becomes wasteful when you start to add multiple boxes to the same config, since you'd be repeating code across the various configs.
