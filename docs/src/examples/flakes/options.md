# Options

Branch: [edu-flake-multibox-options](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-multibox-options)

Prerequisites to Understand:
  * [edu-flake-multibox-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-multibox-basic) ([Writeup](./multi-box.md))

## What is this branch?

When dealing with multiple boxes, you will find that you need to be able to differentiate between them.
The way that I have found to like to do this is through the creation of additional configuration options, which are then set in `hosts/{hostname}/default.nix`.

## Setting the Hostname

We will start off with a very simple new option that lets us set the hostname, just to get the workflow of it down and explain some basic concepts.
This might not be super beneficial since we're currently only using the hostname to set `networking.hostName`, but it gives us something easy to work with.

Since we are going to be using the hostname in `common/default.nix`, that's where it makes sense to create the option we will be setting.

Previously, this file looked a little like this:

```
{ config, lib, modulesPath, ... }:
{
  imports = [
    ...
  ];

  services.xserver.enable = true;
  networking.useDHCP = lib.mkDefault true;
}
```

However, in this branch, we change that up a little and end up with this:

```
{ config, lib, modulesPath, ... }:
{
  imports = [
    ...
  ];

  options.custom.system.hostname = lib.mkOption {
    type = with lib.types; uniq string;
    description = "hostname of the box";
  };

  config = {
    services.xserver.enable = true;
    networking = {
      useDHCP = lib.mkDefault true;
      hostName = config.custom.system.hostname;
    };
  };
}
```

Looking at this, it's important to note that we moved the configurations which were previously at the top level under a new `config` option.
This was done so that we could differentiate between `config` and `options`.

`options` is something we haven't seen yet, but that's where we're going to be creating our custom configuration options.
It should be noted that I chose the namespace `custom`, however, this isn't required to be called `custom`.
It could be anything that makes sense to you.

Once we understand that we're using `options.custom` to hold our configuration options, we can take a look at `system.hostname`, which again, it totally made up by me.
We are using `lib.mkOption` in order to say they this value must be a `string`, and that it must be `uniq` (unique) within the configuration, and if it's defined more than once, an error should be thrown.

We then go to use that new option we created immediately after by saying that `config.networking.hostName` should be set to whatever we set in `cfg.system.hostname`.

Now that all the plumbing is in place, where should we define our new option?
Well, it has been the case that we are using `hosts/{hostname}/default.nix` for things that apply to a single box, so it makes sense that we put it there.

As such, we can have the following in `hosts/primero/default.nix`:

```
config.custom.system.hostname = "primero";
```

Make the appropriate changes for the other hosts, and you now have your first configuration item.
As I said earlier, though, that doesn't really do a whole lot for us since we're really only using the hostname once and we could have just set the hostname directly.

Where this comes into play is when we go to make things more complicated with our configurations and what our options control.

## Increasing Complexity

Now that we understand how we might set and use basic options, we can make things slightly more complicated by using these options to control what packages are available on a system.
Since our boxes are CLI-only at the moment, I'm going to create an option which just installs a few "advanced" tools and a few "compression" related tools.

If we look at the newly created [tools.nix](https://gitlab.com/bytepen/edu/nixos/-/blob/edu-flake-multibox-options/common/tools.nix), we can see that we created two new options; `advanced-cli` and `compression-cli`.

We also did a few new things here.
Looking at the `let ... in` block at the top, we have `cfg = config.custom.tools`.
This allows us to use `cfg.advanced-cli` instead of having to type out `config.custom.tools.advanced-cli` below.
We also have `inherit (lib) mkEnableOption mkIf`, which allows us to call those functions directly without having to type `lib` each time.

Finally, we're doing a weird thing with `systemPackages` that I would like to explain.
First is the usage of mkIf, which evaluates `cfg.advanced-cli`, and only sets the expressions inside of that if it's true.
Second is the use of `++`, which will concatenate one list to another.
Third is the if statement following the `++`.
This leaves us with the following snippit:

```
  config.environment = mkIf cfg.advanced-cli {
    systemPackages = [
      pkgs.tmux
      pkgs.vim
      pkgs.tcpdumpA
      pkgs.tshark
      pkgs.bmon
    ] ++ (if cfg.compression-cli then [ pkgs.zip pkgs.unzip pkgs.bzip3 ] else [ ]);
  };
```

What this works out to is that if `advanced-cli` is set to `true`, then some packages will be added.
Additionally, if `compression-cli` is set to `true`, a few more tools will be added.
If `advanced-cli` is `false`, but `compression-cli` is `true`, no tools will be added because the whole thing is under the `mkIf` for `advanced-cli`.
If either of these are not set, they will default to `false` because that's what `mkEnableOption` does by default.

I also configured the 3 boxes a little differently from each other to help explore this concept.

* [primero](https://gitlab.com/bytepen/edu/nixos/-/blob/edu-flake-multibox-options/hosts/primero/default.nix): Both are `true`, resulting in all tools being added.
* [segundo](https://gitlab.com/bytepen/edu/nixos/-/blob/edu-flake-multibox-options/hosts/segundo/default.nix): `advanced-cli` is set to `true`, but `compression-cli` is undefined, which makes it false. Only the first set of tools are installed.
* [tercero](https://gitlab.com/bytepen/edu/nixos/-/blob/edu-flake-multibox-options/hosts/tercero/default.nix): Neither is defined, so they're both `false` and no tools are installed.
