# Examples

I intend to have a number of branches on [this repository](https://gitlab.com/bytepen/edu/nixos).
Each branch starting with `edu-` is intended to explain/show a specific concept or group or related concepts.
This section aims to explain the branches and caveats of those branches.
