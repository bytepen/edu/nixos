# Introduction

I have spent a number of months bashing my head against NixOS, and while I absolutely love the OS, I understand that the learning curve is absolutely atrocious.
This mdbook serves as my attempt to help others in their journey to using NixOS.

It is accompanied by a GitLab repository that you can find [here](https://gitlab.com/bytepen/edu/nixos).
This repo will have a number of `edu-` branches, each designed to help explain one specific concept.

If you're getting started and are having trouble figuring something out, please try to do your own research, and then reach out to me and I will see about getting it added to this material.


## Notice

I'm still learning NixOS.
Please do not take anything in these docs as gospel and if you find something that is incorrect, please let me know as I'm interested in getting it corrected.
