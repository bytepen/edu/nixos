# Getting Started

One of the main things I'm hoping to get accomplished with this repository is to help people who are new to NixOS understand what's going on.
There is no denying that NixOS has a steep learning curve, but once you begin to understand how to use it, it makes it incredibly trivial to make highly reproducable and reliable systems in a way that no other configuration management system I have tried can.

This section will attempt to cover a number of things to help you get started.
In addition to this resource, I have found a number of other resources which were instrumental to my understanding.
They will be listed below.

## Learning NixOS

- [nix.dev](https://nix.dev/)
- [NixOS Manual](https://nixos.org/manual/nixos/stable/)
- [Ian the Henry: How to Learn Nix](https://ianthehenry.com/posts/how-to-learn-nix/)
- [Xe Iaso: Nix Flakes: an Introduction](https://xeiaso.net/blog/nix-flakes-1-2022-02-21)
- [Xe Iaso: Nix Flakes: Packages and How to Use Them](https://xeiaso.net/blog/nix-flakes-2-2022-02-27)
- [Xe Iaso: Nix Flakes: Exposing and using NixOS Modules](https://xeiaso.net/blog/nix-flakes-3-2022-04-07)

## Advanced Configurations

Xe Iaso also has a ton of other articles on their blog that have served me well, but their articles cover a lot more advanced concepts which may be difficult to understand earlier on.

I am also a fan of deleting everything that NixOS can easily re-create.
Here are a few articles that got me started with getting that set up.

- [mt-caret: Encrypted Btrfs Root with Opt-in State on NixOS](https://mt-caret.github.io/blog/posts/2020-06-29-optin-state.html)
- [Graham Christensen: Erase your darlings](https://grahamc.com/blog/erase-your-darlings/)

## Repositories Used as Inspiration

There are a couple of repositories which I would define as being instrumental to helping me understand how I want to use NixOS.
I will have those listed below.

- [github.com:buckley310/nixos-config](https://github.com/buckley310/nixos-config)
- [github.com:skogsbrus/os](https://github.com/skogsbrus/os)
