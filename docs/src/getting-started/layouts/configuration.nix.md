# Configuration.nix

This is the default and currently supported method of configuring NixOS.
When you are installing a fresh system, you will generally find instructions which indicate that you should run `nixos-generate-config`.
This command generates two files; /etc/nixos/configuration.nix and /etc/nixos/hardware-configuration.nix.

If you are just starting out with NixOS, I encourage you to use configuration.nix until you begin to see how everything fits together, then I would recommend to switch over to flakes once you start to feel that configuration.nix is getting too big and you want to start splitting things out.

For the purpose of this guide, I have created the following branch to see what these files would look like upon initial generation:

[edu-configuration-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-configuration-basic)

## configuration.nix

This file is the "source of truth" when it comes to your configuration.nix based installation.
You can start from here and follow the options within through to every other files which is used in the configuration.

### imports

You may notice that there is an `imports` attribute in the `configuration.nix` file, which specifies that `./hardware-configuration.nix` should be imported.

One important thing to know about NixOS is that you can split out your configurations across as many files as feels necessary to you.
This can increase the complexity of your configuration as you may lose track of where exactly things are, but it is also a very powerful tool that you can use to help yourself stay organized.

## hardware-configuration.nix

This file contains things which are specific to the physical hardware running your NixOS installation.
This includes things like the Kernel Modules that should be expected to be used based on what types of hardware is present (NIC chipsets, audio chipsets, processor type, etc.) as well as the partitions found which will be used for booting your OS.

It comes with advice to not modify that file, which is generally good advice if you intend to manage that file through `nixos-generate-config`, however, I often take a different approach in which all of my systems have their OS drives formatted in the exact same way and have their kernel modules specified through other mechanisms.
With that said, I will still run `nixos-generate-config --dir /tmp` on my various boxes from time to time just to get that output and make sure that everything is still configured as it should be.
