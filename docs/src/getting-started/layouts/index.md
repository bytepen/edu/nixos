# NixOS Layouts

When it comes to the structure of your `/etc/nixos`, there are two prevailing layouts.
I will discuss and compare these within this section.

* [Flakes](./flakes.md): This is the "unsupported/experimental" layout, however, everyone is switching over to it and I personally find that it makes thigns a lot cleaner. With that said, I would probably recommend playing around using the configuration.nix layout until you feel like you have a solid understanding of what NixOS is doing and how it works.
* [Configuration.nix](./configuration.nix.md): This is the "supported" layout and it probably the simplist to get started with, however, I prefer the way flakes look over configuration.nix and there are some powerful features in flakes which are not as easily implemented in configuration.nix based layouts.

I found that the real tipping point for me in going from configuration.nix to Flakes was when I wanted to start considering how to manage multiple boxes with the same set of configuration files.
This CAN be done with a configuration.nix approach, but I really like how I have mine set up with Flakes.

When it comes down to understanding how someone elses NixOS Configuration works, it is very important to understand the roles of two files; flake.nix and configuration.nix.
Every NixOS Configuration should have ONE of these two files, and these files serve as the base for the entire rest of the configuration.
By examining the configuration.nix or flake.nix, you are able to understand what additional files present within the NixOS Configuration are imported and under what circumstances.
