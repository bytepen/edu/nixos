# Flakes

In contrast to `configuration.nix`, we are starting to see the increase in popularity of Flakes.
While Flakes are still experimental, they offer a number of tools that can help us stay organized and apply one NixOS configuration across a number of boxes.

It should be noted that having a flake.nix largely means that Flakes are being used, and that the flake.nix is expected to take `configuration.nix`'s place as being the "source of truth"

For the purpose of this guide, I have created the following branch to show what the default `configuration.nix` file might look like if it were replaced by `flake.nix`.

[edu-flake-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-basic).

## edu-flake-basic vs edu-configuration-basic

If you compare my [edu-flake-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-flake-basic) branch against my [edu-configuration-basic](https://gitlab.com/bytepen/edu/nixos/-/tree/edu-configuration-basic) branch, you will see just a few things were changed. I will go into those changes in this section.

The biggest change right off the bat is that there is no more `configuration.nix` file.
Instead, there is a `flake.nix` file.
If you take a look at this file, you will see that there are 3 high level elements defined; description, inputs, and outputs.

Description explains itself well enough.
Inputs are used to define the dependencies of the flake, while outputs are used to define the things that come out of the flake.

In this example, there are two inputs; nixpkgs, and nixpkgs-unstable.
These are used to define where the system we are configuring will get its packages.

In the outputs section, we begin by passing along the inputs via the `{ self, nixpkgs, nixpkgs-unstable, ... }` snippit.
We then state that the thing that will come out of this flake is a `nixosConfiguration` for a box with the hostname `primero`.
This box is going to be `x86_64-linux` based and its NixOS configuration is going to be over in `./hosts/primero`.

If we take a look at `./hotss/primero`, we will see two files which, upon inspection, will look very familiar; default.nix and hardware-configuration.nix.
You may notice that `default.nix` is identical to the default `configuration.nix`.
I have done this to demonstrate the utility of `default.nix`.
When importing files, you can leave off the exact file to import so long as that file is called `default.nix`.

What this means is that `./hosts/primero` and `./hosts/primero/default.nix` would both import the same file.
Alternatively, if you wanted to leave it named `configuration.nix`, that's fine too, but you would need to specify the module in `flake.nix` as `./hosts/primero/configuration.nix`.

You will also notice that I did add one attribute to `default.nix`. This is the `networking.hostName` attribute.
When dealing with flakes, it is MUCH easier (though technically not required) for you to correlate your nixosConfigurations with the hostname of the box you're configuring.

The reason for this is that you can simply install with the following command:

```
sudo nixos-rebuild boot
```

Whereas, if you didn't have a flake output for the system you are trying to configure, you would need to execute it in the following way:

```
sudo nixos-rebuild boot --flake /etc/nixos/#primero
```

As such, it makes sense to just set the hostname directly in the config and make sure that you have a corresponding entry in the flake.nix.
