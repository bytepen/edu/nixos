# Installation

The Official Installation Guides can be found [here](https://nixos.wiki/wiki/NixOS_Installation_Guide) and [here](https://nixos.org/manual/nixos/stable/index.html#ch-installation), but I figured I might as well go over the installation in general and add any notes I feel are relevant.

I have a somewhat complicated script [here](https://gitlab.com/bytepen/nixos/public/-/blob/main/install.sh), but it is very far from generic and is highly specific to the way that I have my NixOS configurations set up.
Its utility to you varies heavily based on your ability to follow it.

## General Installation Steps

1. Prepare the OS Drive
1. Add the NixOS Config
1. Install NixOS

### Prepare the OS Drive

This should be pretty straight forward to anyone who has manually installed an OS before.

I tend to split the drive into two partitions, one for `/boot`, and one for `/`. 
This can be seen in the `format_system_disk` function of my install script mentioned above.

From there, I LUKS encrypt the partition that will be used for `/`, and use the mkfs utils to set the `/boot` partition to fat and the `/` partition to btrfs as seen in the `mkfs_system_disk` function of my install script.

After that, I create and mount a number of btrfs subvolumes as seen in `create_system_subvolumes` and `mount_system_disks`.
Note that some of my subvolumes are created the way they're created because I wipe `/` on every boot.
This is likely irrelevant to you unless you are going for the same setup, so just create the subvolumes you need instead of blindly following my script.

### Add the NixOS Config

Now that the drive is ready and mounted, you can execute the following command to generate the config:

```
sudo nixos-generate-config
```

This will create the following files:

* `/mnt/etc/nixos/configuration.nix`: Configuration options relevant to your OS installation
* `/mnt/etc/nixos/hardware-configuration.nix`: Configuration options relevant to your hardware

If you are just starting out, I highly recommend to leave everything as it is, and immediately jump into the next step.
NixOS maintains a list of previous configurations that you can choose at boot, and this default configuration is basically guaranteed to succeed and boot into a CLI where you can make the rest of your changes, install them, then easily rollback when the box fails to boot.

### Install NixOS

With your `/mnt/etc/nixos/configuration.nix` in place, you can execute the following command to begin the installation:

```
sudo nixos-install
```

### What now?

Now that you've got a computer that's booted into a very basic NixOS configuration, you can start tweaking things.

I recommend starting out VERY simply using some of the information in other parts of this guide.
If you start trying to configure everything all at once, it is very easy to become overwhelmed.

Additionally, I HIGHLY recommend you create a Git repository to track your configuration, that way, if things go horribly wrong, you have a place to fall back to instead of having to start all over.
NixOS has a very steep learning curve, and while it would technically be possible to play through Dark Souls without a save state, do you actually want to?
