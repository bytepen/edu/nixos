# Versioning

## How does the release versioning system work in NixOS?

One thing that can be a little bit confusing to people is how exactly the versions of NixOS work.

Versions of NixOS are known as "Channels", and there are 2 or 3 "supported" channels at any time.
As of the time of writing this (17JUN2023), there are currently 3 supported channels; 22.11, 23.05, and unstable.

The first number in a channel corresponds to the year, while the second number corresponds to the month the channel was released.
For example, 23.05 was released in May of 2023.

Channels aim to be released about every 6 months, and the previous channel recieves security updates for about a month after the latest one is released.
With this in mind, you should aim to upgrade to the current channel quickly after it is released.

When you are installing a fresh install, your valid options are the current numbered channel, or the "unstable" channel, though you may find yourself on an unsupported channel if you have had it installed for a while.
In this case, you will want to update to the latest channel as explained below.

You can find a list of all channels and their support status [here](https://endoflife.date/nixos).

## How are channels managed?

NixOS channels are maintained on the [Nixpkgs GitHub Repository](https://github.com/NixOS/nixpkgs/).

As of the time of this writing (17JUN2023), there are two very relevant branches.

* [nixos-unstable](https://github.com/NixOS/nixpkgs/tree/nixos-unstable): Corresponds to the "unstable" channel
* [nixos-23.05](https://github.com/NixOS/nixpkgs/tree/nixos-23.05): Corresponds to the "23.05" channel

Most new development happens on the "unstable" branch.
This is where you will find the most "bleeding edge" packages.
The 23.05 channel, on the other hand, will recieve backports of relevant software at a slightly slower pace, and may be a little out of date, but still fairly relevant.
As an example, something huge like the linux kernel may have the same version between 23.05 and unstable, but a smaller, frequently updated piece of software may be a version or two behind on 23.05 while the latest version would be available on unstable.

This difference becomes more visible as you near the next versioned number and package maintainers start worrying less about backports as they begin to focus on the next channel.
After the next channel is released, the previous channel will definitely fall behind.

When it comes time for a new channel to take the place of an old channel, the nixos-unstable branch is split off onto the nixos-XX.YY branch, and the nixos-XX.YY is put through a number of checks before it is deemed to be the current channel.
You can see what this process and timeline looked like for 23.05 [here](https://github.com/NixOS/nixpkgs/issues/223562).

## Which channel should I choose?

This really comes down to the question "Do you need the bleeding edge and are you willing and able to sustain the cuts?"

For me, the answer to this is "no," and as such, I prefer to stay on the latest numbered channel.
However, you may decide that's what you want to do, in which case you could choose the unstable channel.

Keep in mind that it is possible to use specific packages from the unstable channel, even if you aren't using that channel for everything.
So if I need the bleeding edge version of a specific package, I can install that package from the unstable branch on my largely stable channel installation.

## How do you upgrade to a new channel?

I'll keep this geared towards Flake-based installations, as I switched over to flakes pretty early in my journey, and only have experience upgrading there.
If you're unfamiliar with the difference between Flake and configuration.nix based installations, I recommend you explore those sections of this guide first.

Within the `inputs` section of your `flake.nix` file, you will need to modify the input corresponding to your nixpkgs to have the new URL, then just run `sudo nixos-reconfigure boot` while fixing problems it brings up until it stops yelling at you.
I was able to upgrade from 22.11 to 23.05 in less than an hour.

It is also worth noting that there are a few options (such as `config.system.stateVersion`), which may have older channel versions.
This is fine as these options help NixOS read your config, and do not influence the actual version used.
You can change this as well, but it may introduce a number of problems due to configuration options having changed between channels.
In general, it is recommended to leave these options the same forever unless you have the time/desire to fix a ton of broken things.
