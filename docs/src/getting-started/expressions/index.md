# NixOS Expressions

The contents of a `.nix` file is called a NixOS Expression.

## Structure

Nearly every `.nix` file will follow the following structure:

```
{
  // inputs
  ...
}: 
{
  // outputs
  some.output.option = true;
}
```

The `inputs` are used define what exactly will be used within the `outputs` section.
As an example, if you are going to have the line `environment.systemPackages`, you are likely going to be specifying packages from the `pkgs` input, and as such, you will have errors if you fail to include `pkgs` in your `inputs`.
An example of what this might look like is as follows:

```
{ pkgs, ...}:
{
  environment.systemPackages = [
    pkgs.tmux
  ];
}
```

## imports

You may notice that there is an `imports` attribute in the `configuration.nix` file, which specifies that `./hardware-configuration.nix` should be imported.

One important thing to know about NixOS is that you can split out your configurations across as many files as feels necessary to you.
This can increase the complexity of your configuration as you may lose track of where exactly things are, but it is also a very powerful tool that you can use to help yourself stay organized.
