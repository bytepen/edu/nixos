# Commands

There are a few commands which are SUPER handy to know when learning/using NixOS.
I will be documenting some of them here.

## `nixos-rebuild`

This command is frequently used when using NixOS.
Its primary use is to update the installation with changes which have been made to the NixOS Configuration.

* `nixos-rebuild switch`: Apply the NixOS Configuration immediately, which may break the environment you are currently using.
* `nixos-rebuild boot`: Apply the NixOS Configuration at the next boot. Useful when you're making big changes and don't want to kick yourself out immediately.
* `nixos-rebuild switch --flake /etc/nixos/#${FLAKE_NAME}`: Install the configuration for the flake name stored in `${FLAKE_NAME}`.

## `nix-shell`

* `nix-shell -p ${PACKAGE_1} ${PACKAGE_2}`: There may be times when you want to use a program for just a few moments, but don't necessarily need to access that program at all time. For example, while writing these documents, it's useful to have access to the `mdbook` binary, but it isn't a binary that I use a lot, so I could open a terminal, execute `nix-shell -p mdbook`, then I could execute `mdbook serve` within that terminal to use the `mdbook` binary. As soon as I close out of that terminal, the `mdbook` binary is no longer in my ${PATH}.
